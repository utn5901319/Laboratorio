#include <iostream>
#include <stdio.h>
using namespace std;


int main(){
    int positivos = 0, negativos = 0, ceros = 0, num;

    for(int i = 0; i < 20; i++){
        cout<< "Porfavor ingrese un numero: ";
        cin >> num;
        if(num >= 1 ){
            positivos ++;

        }
        else if (num < 0){
            negativos++;
        }
        else{
            ceros++;
        }
    }
    cout << "El porcentaje de numeros positivos son: " << positivos * 100 / 20 << endl;
    cout << "El porcentaje de numeros negativos son: " << negativos * 100 / 20 << endl;
    cout << "El porcentaje de numeros iguales a cero son: " << ceros * 100 / 20 << endl;
    


    return 0;
}