#include <iostream>
#include <stdio.h>

using namespace std;

int main(){
    
    int numero, pos;
    int  impares = 0;

    for (int y = 0; y < 2; y++){

        cout << endl <<"Empieza el conjunto" << endl << endl;

        int positivos = 0, primo = 0, numprimo = 0; 
        int negativos = 0, ceros = 0;

        for (int x = 0; x < 2; x++){
            int count = 0;
            cout << "Ingrese un numero: ";
            cin >> numero;
            

            if(numero < 0){
                negativos++;
            }
            else if(numero >= 1){
                positivos++;
            }
            else{
                cout << numero;
                ceros++;
            }
            for (int i = 1; i < numero + 1; i++){
                if (numero % i == 0 ){
                    count++;
                }
            
            }
            if (count == 2){
                pos = x;
                numprimo = numero;

                }
            if (numero % 2 != 0){
                impares++;
            }   
            
        }
        cout << endl << "Finaliza el conjunto" << endl << endl;

        cout << endl << "A)" << endl << endl;

        cout << "el conjunto numero " << y + 1;
        cout << ", La cantidad de numeros positivos son:  " << positivos << endl;
        cout << "La cantidad de numeros negativos son:  " << negativos << endl;
        cout << "La cantidad de numeros iguales a cero son:  " << ceros << endl;

        if (numprimo == 0){ 
                cout << "No hay ningun numero primo";
        }
        else{
            cout << endl << "B)" << endl << endl;
            cout << "El ultimo numero primo del conjunto es: " << numprimo << " y su posicion es: " << pos << endl;    
        }    

    }

    cout << endl << "C)" << endl << endl;
    cout << "La cantidad de numeros impares son: " << impares << endl;

    return 0;
}