#include <iostream>
#include <stdio.h>
using namespace std;

int main(){


    int primer = 0, ultimo = 0, numero;

    for(int i = 0; i < 7; i++){
        cout << "Ingrese un numero: ";
        cin >> numero;
        if (primer == 0 && numero % 2 != 0){
            primer = numero;
        }
        else if(primer != 0 &&  numero % 2 != 0){
            ultimo = numero;    
        }
        else{
            cout << "No se han ingresado los suficientes numeros impares" << endl;
        }

    }
    
    cout << "El primer numero impar es: " << primer << endl;
    cout << "El ultimo numero impar es: " << ultimo << endl;
    

    return 0;
}