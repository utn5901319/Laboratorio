#include <iostream>
#include <stdio.h>
using namespace std;
/*
La proveeduría Los Primos Gazpacho registró la información del stock de sus productos más exitosos en cuanto a ventas. El registro de cada producto se compone de:

    Código de Producto (entero)

    Tipo de producto ('B' - Bebida,  'S' - Snack,  'F' - Fiambre)

    Stock Disponible (entero)

    Precio unitario (float)


La información no se encuentra agrupada ni ordenada. Hay un total de 5 productos. Se le pide calcular e informar:

    El producto con el mayor precio de tipo fiambre, informando su código de producto y su stock.

     El porcentaje de unidades en stock de cada tipo de producto.

    El tipo de producto con más cantidad de productos.
    */
int main(){ 

    int productcode, stock;
    float price;
    char typeproduct;
    int maxproductcode, maxstock, maxprice;
    int stockTotal, fcountStock, bcountStock, scountStock;


    for (int i = 0; i < 5 ; i++){

        cout << "Ingrese el codigo del producto: ";
        cin >> productcode;

        cout << "Ingrese el tipo de producto: ";
        cin >> typeproduct;

        cout << "Ingrese el stock disponible: ";
        cin >> stock;

        cout << "Ingrese el precion unitario: ";
        cin >> price;

        if((typeproduct == 'f') || (typeproduct == 'F')){
            fcountStock++;
            if (i == 0){
                maxproductcode = productcode;
                maxstock = stock;
                maxprice = price;
            }
            else{
                if (price > maxprice){
                    maxproductcode = productcode;
                    maxstock = stock;
                    maxprice = price;
                }
            }
        }

        else if((typeproduct == 'b') || (typeproduct == 'B')){
            bcountStock++;

        }
         else if((typeproduct == 'b') || (typeproduct == 'B')){
            scountStock++;

        }
        else{
            cout <<"Tipo de producto incorrecto" << endl;
        }
        
                
    }
    stockTotal = scountStock + bcountStock + fcountStock;

    fcountStock = fcountStock * 100 /stockTotal;

    bcountStock = bcountStock * 100 /stockTotal;

    scountStock = scountStock * 100 /stockTotal;

    

    cout << endl << endl << "A)" << endl;

    cout << endl << endl << "B)" << endl;
    
    cout << endl << endl << "C)" << endl;
        

    
    return 0;
    }