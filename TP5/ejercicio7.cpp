#include <iostream>
#include <stdio.h>
/*
    7) Hacer un programa para ingresar los valores de los pesos de distintas encomiendas que
se deben enviar a distintos clientes y que finaliza cuando se ingresa un peso negativo. Se
deben agrupar las encomiendas en camiones que pueden transportar hasta 200 kilos en
total.
    Se pide determinar e informar:
    a) El número de cada camión y peso total de encomiendas. Camión 1: 170 kg, Camión 2:
170 kg, etc.
    b) El número de camión que transporta mayor cantidad de encomiendas. En el ejemplo
anterior sería el Camión 3 con 4 encomiendas.
*/

int main(){
    using namespace std;

    int peso;
    int total, maxtotal;
    int count  = 0, encomiendas;
    int maxencomiendas, maxcamion;
    int fstvuelta = 1;

    do{ 
        
        count++;
        
        cout << endl << endl << "A)" << endl;
        cout << "Camion: " << count << endl;
        
        total = 0; 
        encomiendas = 0;

        do {
            
            cout << "ingrese el peso de la encomienda: ";
            cin >> peso;
            
            encomiendas++;

            total += peso;

            }while((total < 200) && (peso > 0));    

        if (fstvuelta == 1){
                fstvuelta = 0;
                maxencomiendas = encomiendas;
                maxcamion = count;
            }
        else{

            if(encomiendas > maxencomiendas){
                maxencomiendas = encomiendas;
                maxcamion = count;
            }
        }

        if (peso < 0){
            continue;
        }
        else{
            cout << endl;
            cout << "Peso total: " << total << "kg" << endl;
        }
        
    

    }while(peso > 0);

    cout << endl << endl << "A)" << endl;
    cout << "El Camion "  << maxcamion << " fue el camion con mayor cantidad de encomiendas" << endl; 
    


    return 0;
}