#include <iostream>
#include <stdio.h>
using namespace std;


/*
    5) Se dispone de una lista de 10 grupos de números enteros separados entre ellos por
ceros. Se pide determinar e informar:

    a) Calcular el promedio de valores de cada grupo.

    b) Determinar e informar el valor mínimo de todo los grupos, indicando en que grupo se
encontró y su posición relativa en el mismo.

    c) El mayor de los promedios y a que grupo pertenecía.*/

int main(){

    int numero;
    int count;
    int total;

    int fstvuelta = 1, fstvueltaGrupo = 1;
    int min, mingroup, minpos, maxpromedioGroup;

    float promedio, maxpromedio;
    
   
    for (int i = 0; i < 10; i++){

        count = 0;
        total = 0;

        do{
            
            cout << "Por favor ingrese un numero: " ;
            cin >> numero;

            // evita que el 0 cuente como un numero/posicion
            if (numero == 0){
                continue;
            }
            else{

                // contador para la posicion
                count++;

                // Bandera para la primera vuelta
                if (fstvuelta == 1){

                    min = numero;  
                    mingroup = i + 1;
                    minpos = count;

                    fstvuelta = 0;
                }

                // asignar valor minimo
                else{
                    if (numero < min){
                        min = numero;  
                        mingroup = i + 1;
                        minpos = count;
                    }
                }
            }

            
            
            total += numero;


        }while(numero !=0);

        
        // Calcular promedio del grupo
        if (count == 0){
            promedio = 0;
        }
        else{
            promedio = total / count ;
        }

        // checkea y asigna el grupo con mayor promedio
        if (fstvueltaGrupo == 1){
            fstvueltaGrupo = 0;
            maxpromedioGroup = i + 1;
            maxpromedio = promedio;
        }
        else{
            if (promedio > maxpromedio){
                maxpromedioGroup = i + 1;
                maxpromedio = promedio;
            }
        }

        cout << endl << endl << "A)" << endl;
        cout << "El promedio del grupo " << i + 1 << " es: " << promedio << endl;
    }

    cout << endl << endl << "B)" << endl;
    cout << "El numero minimo de todos los grupos es: " << min << ",  su grupo es: " << mingroup << " y su posicion es: " << minpos << endl;

    cout << endl << endl << "C)" << endl;
    cout << "El mayor promedio es: " << maxpromedio << " y su grupo es el: " << maxpromedioGroup << endl;




    return 0;
}