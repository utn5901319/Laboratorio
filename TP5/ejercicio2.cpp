#include <iostream>
#include <stdio.h>

/*
    2) Dada una lista de números compuesta por 10 grupos y cada grupo separado del
siguiente por un cero, se pide determinar e informar:

    a) Para cada uno de los grupos el máximo de los números pares y el máximo de los
números impares.

    b) Para cada uno de los grupos el porcentaje de números negativos y números positivos.

    c) Cuantos números positivos había en total entre los 10 grupos.
*/

using namespace std;

int main(){ 
    int numero, maxpar, maximp, fstnumpar, fstnumimp, pares, impares, cantnumeros, positivos, negativos, positivostotal = 0;

    for(int i = 0; i < 2; i++){
        cout << "Grupo " << i + 1 << endl;

        negativos = 0;
        positivos = 0;

        fstnumpar = 1;
        fstnumimp = 1;

        impares = 0; 
        pares = 0;

        cantnumeros = 0;

        do{
            cout << "Ingrese un numero: ";
            cin >> numero;

            // A) 
            if (numero % 2 == 0){

                // contador de numeros pares
                pares++;

                // Asignacion de numero maximo par
                if (fstnumpar == 1){
                    fstnumpar == 0;
                    maxpar = numero;
                }
                else{
                    if (numero > maxpar){
                        maxpar = numero;
                    }
                }
            }


            else{

                // Contador de numeros impares
                impares++;

                // Asignacion de numero maximo impar
                if (fstnumimp == 1){
                    fstnumimp = 0;
                    maximp = numero;
                }
                else{
                    if (numero > maximp){
                        maximp = numero;
                    }
                }
            }

            // Contador numeros positivos
            if(numero >= 0){
                positivos++;
                positivostotal++;
            }  

            // Contador numeros negativos
            else {
                negativos++;
            }

            // contador de el total de numeros
            cantnumeros++;

        }while(numero != 0);

        cout << endl << endl << "A)" << endl;

        if (fstnumpar == 1){
            cout << "No se a ingresado ningun numero par"<< endl;
        }
        else  {
            cout << "EL maximo de los numeros pares es: " << maxpar << endl;
        }

        if (fstnumimp == 1){
            cout << "No se a ingresado ningun numero impar"<< endl;
        }

        else  {
            cout << "EL maximo de los numeros impares es: " << maximp << endl;
        }

        cout << endl << endl << "B)" << endl;
        cout << "El porcentaje de numeros positivos es: " << positivos * 100/ cantnumeros << "%"<< endl;
        cout << "El porcentaje de numeros negativos es: " << negativos * 100/ cantnumeros << "%"<< endl;

        cout << endl << endl << "C)" << endl;
        cout << "La cantidad de numeros positivos son: " << positivos << endl;
        
    }



    return 0;
}
