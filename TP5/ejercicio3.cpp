#include <iostream>
#include <stdio.h>
using namespace std;
/*
    3) Se dispone de una lista de 10 grupos de números enteros separados entre ellos por
ceros. Se pide determinar e informar:

    a) El número de grupo con mayor porcentaje de números impares positivos respecto al total
de números que forman el grupo.

    b) Para cada grupo el último número primo y en qué orden apareció en ese grupo, si en un
grupo no hubiera números primos informarlo con un cartel aclaratorio.

    c) Informar cuantos grupos están formados por todos números ordenados de mayor a
menor.

*/


int main(){ 
    int cantnumeros, divisores, countnums, ordenado;
    int numero, ultnumero;
    int imppos, porcimppos, fstimpgroup = 1, maxporcimppos, groupmaximp;
    int posnumprimo, numprimos, ultnumprimo;
    

    for(int i = 0; i < 2; i++){

        // Muestra el numero del grupo
        cout << "Grupo " << i + 1 << endl;

        // Declaracion de variables utilizadas por grupo
        porcimppos = 0;
        imppos = 0;

        cantnumeros = 0;
        divisores = 0;
        countnums = 0;
        ordenado = 0;

        numprimos = 0;
        ultnumprimo = 0;
        
        
        
        // do while para el ingreso del numero y evaluacion del mismo
        do{
            divisores = 0;
            countnums++;

            cout << "Ingrese un numero: ";
            cin >> numero;

            // Verifica orden mayor a menor 
            if (ultnumero > numero){
                ordenado = 1;
            }
            else{
                ordenado = 0;
            }

            

            if ( numero % 2 != 0) {
                // Suma de numeros impares positivos
                if (numero > 0){
                    imppos++;
                }
            }

            // suma la cantidad de numeros
            cantnumeros++;

            // contar los divisores de un numero
            for (int j = 1; j <= numero; j++){
                if( numero % j == 0){
                    divisores++;
                }
            }

            // asignacion de posicion de numero primo y contador de cantidad de numeros primos
            if (divisores == 2){
                cout << numero << endl;
                numprimos++;
                posnumprimo = countnums;
                ultnumprimo = numero;
            }

            
            cout << divisores << endl;
            ultnumero = numero;
        }while(numero != 0);
        // Porcentaje numeros impares positivos en el total de numeros
        porcimppos = imppos * 100/cantnumeros;

        // Asigna el maximo de porcentaje de numeros impares positivos en el primer grupo
        if (fstimpgroup == 1){
            groupmaximp = i + 1;
            maxporcimppos = porcimppos;
        }

        // Asigna el maximo de porcentaje de numeros impares positivos
        else{
            if (porcimppos > maxporcimppos){
               groupmaximp = i + 1;
               maxporcimppos = porcimppos;
            }
        }
        

        
        cout << endl << endl << "B)" << endl;

        //revisa la cantidad de numeros primos, por si la posibilidad de que no haya ningun numero primo
        if (numprimos > 0){
            cout << "El ultimo numero primo es: " << ultnumprimo << endl;
            cout << "La posicion del ultimo numero primo es: " << posnumprimo << endl;
        }
        else {
            cout << "En este grupo no se a ingresado ningun numero primo" << endl;
        }
        

        cout << endl << endl << "C)" << endl;
        if(ordenado == 1){
            cout << "El grupo esta ordenado de mayor a menor." << endl;
        }
        
    }
    
    cout << endl << endl << "A)" << endl;
    cout << "El grupo con mayor porcentaje de numeros primos es el grupo : " << groupmaximp << endl;




    return 0;
}
