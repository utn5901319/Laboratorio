#include <iostream>
#include <stdio.h>

using namespace std;
/*
    8) Dada una lista de números compuesta por grupos donde cada grupo está separado del
siguiente por un cero, y la lista de números finaliza cuando se ingresan dos ceros
consecutivos, se pide determinar e informar:

    a) La cantidad de grupos en los que se detecten un total de 4 o más números primos
consecutivos. Se informa 1 resultado al final.

    b) Para cada uno de los grupos en los que se haya detectado números negativos, el mayor y
el segundo mayor de los mismos y en que posición estaban dentro del subgrupo. En los
grupos sin negativos, informar “Grupo sin negativos”. Se informan 4 resultados por cada
grupo.

    c) El mayor número primo de todos los grupos, informando además en que grupo y en qué
posición del mismo fue detectado. Se informan 3 resultados al final.

*/

int main(){
    

    int numero, ultnumero = 1;

    // primos
    int numprimo, ultnumprimo, maxprimo, posprimo, grupoprimo;

    // ceros
    int fstcero = 1;

    // contadores
    int count = 0, divisores, primoconsec, grupoconsec = 0, posicion;

    // negativos
    int  maxnegativo, ultmaxnegativo, negposicion, maxnegposicion;

    // banderas
    int fstnegativo = 1, fstprimo = 1;

    do{

        primoconsec = 1;

        count++;
        posicion = 0;

        fstnegativo = 1;

        cout << "Grupo: " << count << endl;
        do{
            divisores = 0;
            posicion++;

            cout << "Por favor ingrese un numero: ";
            cin >> numero;
            
            if (numero < 0) {
                ultmaxnegativo = maxnegativo;
                maxnegposicion = negposicion;
            }
            
            // verificacion para primer 0
            if (numero == 0){
                if (fstcero == 1){
                    fstcero = 0;
                }
                else{
                    ultnumero = numero;    
                }
            }
            else{
                ultnumero = numero;
            }

            // Cantidad de divisores del numero
            for (int i = 1; i <= numero; i++){
                if (numero % i == 0){
                    divisores++;
                }
            }
            
            // Verificar si el numero es un numero primero
            if (divisores == 2){
                numprimo = numero;

                // Checkea si el ultimo y anteultimo numero primo son consecutivos
                if (fstprimo == 0){
                    
                    if(numprimo > ultnumprimo){
                        primoconsec++;
                    }
                }
            
                // Numero maximo primo, su posicion y grupo
                if (fstprimo == 1){
                    fstprimo =  0;
                    maxprimo = numprimo;
                    posprimo = posicion;
                    grupoprimo = count;
                }
                else{
                    if(numprimo > maxprimo){
                        maxprimo = numprimo;
                        posprimo = posicion;
                        grupoprimo = count;
                    }
                }
            }

            ultnumprimo = numprimo;

            // Si el numero es negativo
            if(numero < 0){
                if (fstnegativo == 1){
                    fstnegativo = 0;
                    maxnegativo = numero;
                    negposicion = posicion;
                }
                else{
                    if (numero > maxnegativo){
                        maxnegativo = numero;
                        negposicion = posicion;
                    }
                }
            }

            
            

        }while(numero != 0);

        if(primoconsec >= 4){
            grupoconsec++;
        }

        cout << endl << endl << "B)" << endl;
        if(fstnegativo == 1){
            cout << "No se a ingresado ningun numero negativo" << endl;
        }
        else{
            cout << "El mayor numero negativo es: " << maxnegativo << endl;
            cout << "Su posicion es: " << negposicion << endl;

            cout << "El anteultimo numero negativo: " << ultmaxnegativo << endl;
            cout << "Su posicion es: " << negposicion << endl;
        }
        


    }while(ultnumero != 0);

    cout << endl << endl << "A)" << endl;
    cout << "La cantida de grupos con 4 o mas numeros primos consecutivos son: " << grupoconsec << endl;
    cout << endl;

    cout << endl << endl << "C)" << endl;
    if( fstprimo == 1){
        cout << "No se a ingresado ningun numero primo" << endl;
    }
    else{
        cout << "El mayor numero primo de todos: " << maxprimo << endl;
        cout << "El gruop en el que se encuentra es: " << grupoprimo << endl;
        cout << "La posicion en la que se encuentra es: " << posprimo << endl;
    }
    
    



    return 0;
    }