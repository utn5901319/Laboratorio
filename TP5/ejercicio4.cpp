#include <iostream>
#include <stdio.h>
using namespace std;
/* 
    4) Dada una lista de números enteros todos distintos entre sí y que finaliza con un cero,
determinar e informar con un cartel aclaratorio si los números primos que aparezcan en la
misma están ordenados de menor a mayor. Los números positivos primos pueden no ser
consecutivos, pero sí estar ordenados.
Por ejemplo:
4, 5, 7, 12, 13, 0 Se emite un cartel que diga “Ordenados” (5,7 y 13)
2, 10, 3, 5, 11, 7, 14, 0 Se emite un cartel que diga “Desordenados” (2, 3, 5, 11 y 7)
*/

int main(){

    int numero;
    int divisores;
    int numprimo, ultnumprimo;
    int fstvuelta = 1;
    int ordenado;

    do {
        
        divisores = 0;
        cout << "Por favor Ingrese un numero: " ;
        cin >> numero;

        // Busca todos los divisores posibles de un numero
        for (int i = 1; i <= numero; i++){
            if (numero % i == 0){
                divisores++;
            }
        }

        // checkea si es un numero primo
        
        if (divisores == 2){
            numprimo = numero;

            // bandera para verificar la primera vuelta
            if( fstvuelta == 1){
                fstvuelta = 0; 
            }
            
            else{

                // verifica si esta ordenado
                if (ultnumprimo < numprimo) {
                    ordenado = 1;
                }
                else{
                    ordenado = 0;
                }
            }
        }

        
        

        

        ultnumprimo = numprimo; 
        
    
    }while(numero != 0);

    // emite por pantalla si el conjunto esta ordenado
    if (ordenado == 0){
        cout << "EL conjunto esta desordenado" << endl;
    }

    else{
        cout << "El conjunto esta ordenado" << endl;
    }



    return 0;   
}