#include <iostream>
#include <stdio.h>


/*
    6) Hacer un programa para ingresar por teclado 10 grupos compuestos por números
ordenados de menor a mayor. El final de cada grupo se detecta al ingresar un número
menor a su anterior.

    Se debe obtener y emitir:

    a) Para cada uno de los grupos la cantidad de números primos que lo componen.

    b) El mínimo número par de cada uno de los grupos.

    c) El anteúltimo y último número positivo de cada uno de los grupos.

*/

int main(){
    using namespace std;

    int numero, ultnumero;

    int fstvuelta = 1;

    int divisores, primos;

    int minpar, fstvueltapar = 1;

    int numpos, ultnumpos;

    // grupos
    for (int i = 0; i < 2 ; i++){

        primos = 0;
        fstvueltapar = 1;
        fstvuelta = 1;

        do {   

            divisores = 0;

            cout << "Por favor ingrese un numero: " ;
            cin >> numero;

            

            // asignacion de cantidad de divisores 
            for (int i = 1; i <= numero; i++){
                if (numero % i == 0){
                    divisores++;
                }
            }
            // contador de numeros primos
            if (divisores == 2){
                primos++;
            }

            // si el numero es positivo
            if (numero > 0 ){

                numpos = numero;

                // si el numero es par
                if (numero % 2 == 0){
                    
                    // primera vuelta de numero par y positivo
                    if (fstvueltapar == 1){
                        fstvueltapar = 0;
                        minpar = numero;
                    }

                    else{

                        // si el numero minimo par es menor al numero par nuevo
                        if (numero < minpar){
                            minpar = numero;
                        }
                    }
                }
            }

            // bandera controla primera vuelta
            if (fstvuelta == 1){
                fstvuelta = 0;
                ultnumero = numero;
            }

            else{

                // asignacion de ultimo numero
                if (numero < ultnumero ){
                    continue;
                    
                }
                
                else{
                    ultnumero = numero;
                }
            }

            ultnumpos = numpos;

            

        }while(numero >= ultnumero );

        
        cout << endl << endl << "A)" << endl;

        cout << "La cantidad de numeros primos del grupo " << i + 1 << " es: " << primos << endl;
        cout << endl;

        cout << endl << endl << "B)" << endl;

        if (fstvueltapar == 1){

            cout << "No se ingreso ningun numero positivo par." << endl;
            cout << endl;

            minpar = 0;
        }
        else{
            cout << "El minimo numero par y positivo es: " << minpar<< endl;
            cout << endl;
        }
        
        cout << endl << endl << "C)" << endl;
        cout << "El ultimo numero positivo es: " << numero<< endl;
        cout << "El anteultimo numero positivo es " << ultnumpos << endl;
        cout << endl;


    }

    


    return 0;
}